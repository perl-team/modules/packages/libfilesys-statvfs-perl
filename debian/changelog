libfilesys-statvfs-perl (0.82-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 22:38:39 +0100

libfilesys-statvfs-perl (0.82-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * debian/watch: use dist-based URL.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Update Vcs-Git field to its canonical value
  * Declare compliance with Debian Policy 3.9.5
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Sun, 13 Apr 2014 18:14:23 +0200

libfilesys-statvfs-perl (0.82-2) unstable; urgency=low

  * Adopting for the Debian Perl Group (Closes: #628164)
  * Refreshed rules and raised debhelper version and compat level to 8
  * Raised standards version to 3.9.2
  * Added dependency on misellaneous dependencies
  * Added debian/source/format
  * Refreshed copyright

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Fri, 29 Jul 2011 22:03:36 +0100

libfilesys-statvfs-perl (0.82-1) unstable; urgency=low

  * The "get well soon for etch" release.
  * New upstream version
  * We no longer provide libfilesys-df-perl, it has its own package now. So
    drop the Provides: and Conflicts: introduced in the previous version.
  * Update debian/watch.

 -- Ingo Saitz <ingo@debian.org>  Fri,  1 Dec 2006 04:42:06 +0100

libfilesys-statvfs-perl (0.75-2) unstable; urgency=low

  * Conflict with libfilesys-df-perl, which is in NEW and provided by us. New
    version scheduled after processing of NEW queue.
  * Bumped to standards version 3.7.2 (no changes).

 -- Ingo Saitz <ingo@debian.org>  Thu, 21 Sep 2006 09:10:51 +0200

libfilesys-statvfs-perl (0.75-1) unstable; urgency=low

  * New upstream version.

 -- Ingo Saitz <ingo@debian.org>  Sat,  2 Apr 2005 03:08:18 +0200

libfilesys-statvfs-perl (0.71-1) unstable; urgency=low

  * New upstream version.
    Upstream changed the package name to Filesys-Statvfs_Statfs_DF.
  * Bumped to standards version 3.6.1 (no changes).
  * Added watch file.

 -- Ingo Saitz <ingo@debian.org>  Fri, 17 Dec 2004 23:27:22 +0100

libfilesys-statvfs-perl (0.68-1) unstable; urgency=low

  * Initial Release. (Closes: #194902)

 -- Ingo Saitz <ingo@debian.org>  Wed, 28 May 2003 16:19:05 +0200
